interface Memory {
    [key: string]: any
}

interface CreepMemory {
    role: string
    state: number
    target: string
    roomTarget: string

    [key: string]: any
    
}
interface RoomMemory {
    scouts: string[]
    lastScout: number

    [key: string]: any
}
interface Source {
    memory: Memory
}
interface Structure {
    memory: Memory
}
interface Room {
    getContainers(): StructureContainer[]
    getLinks(): StructureLink[]
    /**
     * Returns the container associated with the room controller or undefined if there is none
     * @returns {StructureContainer}
     */
    getControllerContainer() : StructureContainer | undefined
    /**
     * 
     * @param {StructureContainer[]} room 
     */
    labelContainers(containers): any
}