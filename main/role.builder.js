module.exports = roleBuilder;

var util = require("./utility");
var utilMaps = require('./utility.map');
var utilCreeps = require("./utility.creeps");

const STATE_SPAWNING = 0;
const STATE_SEARCHINGBUILDINGS = 1;
const STATE_SEARCHINGENERGY = 2;
const STATE_WALKING = 3;
const STATE_BUILDING = 5;
var roleBuilder = {

    /** 
     * genral run function
     * @param {Creep} creep 
     */
    run: function(creep) {
        // check if state is defined
        if(!creep.memory.state) {
            // if not, set spawning as current state
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;
            case STATE_SEARCHINGBUILDINGS:
                this.runSearchingBuildings(creep);
                break;
            case STATE_SEARCHINGENERGY:
                this.runSearchingEnergy(creep);
                break;
            case STATE_WALKING:
                this.runWalking(creep);
                break;
            case STATE_BUILDING:
                this.runBuilding(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if initialized
        if(!creep.memory.init) {
            // if no, init this creep

            creep.memory.init = true;
        }
        // check if creep is still spawning
        if(!creep.spawning) {
            // if no, change to next state and run it
            creep.memory.state = STATE_SEARCHINGENERGY;
            this.run(creep);
        }
    },
    /**
     * function while searching for things to build
     * @param {Creep} creep 
     */
    runSearchingBuildings: function(creep) {
        let targets = creep.room.find(FIND_CONSTRUCTION_SITES);
        if(targets.length>0) {
            creep.memory.targetAction = "build_0";;
            creep.memory.target = targets[0].id;

        }
        let defensiveBuilding;
        // no contruction jobs- build ramparts and walls
        // ramparts are more important cause they decay
        if(creep.memory.target == undefined) {
            /** @type {StructureRampart} */
            let defensiveBuilding = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (structure) => {
                return structure.structureType == STRUCTURE_RAMPART && structure.hits < 40000;
            }});
            if(defensiveBuilding != undefined) {
                creep.memory.targetAction = "repair_40000";
                creep.memory.target = defensiveBuilding.id;
            }
        }
        // then walls
        if (creep.memory.target == undefined ) {
            let defensiveBuilding = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_WALL && structure.hits < 10000;
                }
            });
            if(defensiveBuilding != undefined) {
                creep.memory.targetAction = "repair_10000";
                creep.memory.target = defensiveBuilding.id;
            }
        }
        // then global construction projects
        if (creep.memory.target == undefined ) {
            let globalConstructs = Game.constructionSites;
            let globalKeys = Object.keys(globalConstructs);
            if(globalKeys.length>0) {
                console.log("searching constructs");
                creep.memory.target = globalKeys[0];
                creep.memory.targetAction = "build_0";
            }
        }
        if (creep.memory.target != undefined) {
            creep.memory.state = STATE_WALKING;
            this.run(creep);
        } else {
            // if no suitable target found, move to parking spot if there is one
            if(utilMaps.getFlagsForRoom(creep.room.name).includes("PARK")) {
                creep.moveTo(Game.flags[creep.room.name+"_PARK"].pos);
            } else {
                creep.moveToHome();
            }
        }

    },
    /**
     * function while searching for energy
     * @param {Creep} creep 
     */
    runSearchingEnergy: function(creep) {
        if(creep.room.storage != undefined) {
            util.collectFromStorage(creep,creep.carryCapacity,5000);
        } else {
            util.collectNearestEnergy(creep);
        }
        if(creep.store[RESOURCE_ENERGY] == creep.store.getCapacity()) {
            // if no, change to next state and run it
            creep.memory.state = STATE_SEARCHINGBUILDINGS;
            this.run(creep);
        }
    },
    /**
     * function while walking to target
     * @param {Creep} creep 
     */
    runWalking: function(creep) { 
        let target = Game.getObjectById(creep.memory.target);
        creep.moveTo(target);
        if(creep.pos.inRangeTo(target,3)) {
            // if no, change to next state and run it
            creep.memory.state = STATE_BUILDING;
            this.run(creep);
        }
    },
    /**
     * function while building
     * @param {Creep} creep
     */
    runBuilding: function(creep) {
        /** @type {Structure|ConstructionSite} */
        let target = Game.getObjectById(creep.memory.target);
        // console.log(target);
        if(target != undefined) {

            let targetActions = creep.memory.targetAction.split("_");
            let targetAction = targetActions[0];
            let targetActionAmount = targetActions[1];
            if(targetAction == "build") {
                // console.log("building");
                creep.build(target);
            } else if(targetAction == "repair") {
                // console.log("repairing");

                if(target.hits<targetActionAmount) {
                    creep.repair(target);
                } else {
                    creep.memory.target = undefined;
                }
            }
        } else {
            creep.memory.target = undefined;
        }
        if(creep.store[RESOURCE_ENERGY] == 0 || creep.memory.target == undefined) {
            // if no, change to next state and run it
            creep.memory.target = undefined;
            creep.memory.state = STATE_SEARCHINGENERGY;
            this.run(creep);
        }
    },
    /**
     * 
     * @param {StructureSpawn} spawn 
     */
    build: function(spawn) {
	    if(     spawn.room.energyCapacityAvailable>600) {
	        spawn.createCreep([WORK,WORK,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined,
                        {role: 'builder', empty: true, sleep: false, building: false, homeRoom : spawn.room.name});
	    }
	    else if(spawn.room.energyCapacityAvailable>450) {
	        spawn.createCreep([WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined,
                        {role: 'builder', empty: true, sleep: false, building: false, homeRoom : spawn.room.name});
	    }
	    else if(spawn.room.energyCapacityAvailable>350) {
	        spawn.createCreep([WORK,CARRY,CARRY,MOVE,MOVE,MOVE], undefined,
                        {role: 'builder', empty: true, sleep: false, building: false, homeRoom : spawn.room.name});
	    } else {
	        spawn.createCreep([WORK,CARRY,MOVE,MOVE], undefined,
                        {role: 'builder', empty: true, sleep: false, building: false});
	    }
	},
	sleepAll: function() {
    	_.filter(Game.creeps,(creep) => creep.memory.role=='builder').forEach(function(creep) {
    		creep.memory.sleep=true;
		})
	},
};
module.exports = roleBuilder;