// @ts-check
// Roles
var roleNames = ["harvester","miner","extractor","commandSupplier","towerSupplier",
"builder","hauler","miningHauler","containerHauler","distributor","maintenance",
"upgrader","scout","reserver"];
var roles = {};
for(var role of roleNames) {
    roles["role_"+role] = require("role."+role);
}

// Prototypes
var prototypeNames = ["spawn","container","creep","structure","room","link","source","storage"];
for(var prototype of prototypeNames) {
    require("prototype."+prototype)();
}
// Util
var _ = require('lodash');
var util = require('./utility');
var extUtil= require("./utility.map");
var codeExtension = require("./codeExtension");
var population = require('./populationControl');
var creepFactory = require("./creepFactory");
var memoryManager = require('./memoryManager');
var roomManager = require('./roomManager');
var roomScoutManager = require('./roomScoutManager');
var roomReservationManager = require('./roomReservationManager');
var roomHarvestManager = require('./roomHarvestManager');

// PROFILER--comment in
// var profiler = require('profiler');
// profiler.enable();
// Extend Code
// codeExtension.extendPrototypes()
try {
        codeExtension.extendPrototypes();
    } catch(e) {
        console.log("extension"+e);
    }

module.exports.loop = function () {
    // ********Main Run Loop
    // console.log("tick")
    
    // PROFILER--comment in
    // profiler.wrap(function() {
    for (const flagName in Game.flags) {
        const flag = Game.flags[flagName];
        if(flagName.includes("_RESERVING")) {
            roomReservationManager.check(flag.pos.roomName);
        }
        if(flagName.includes("_HARVESTING")) {
            roomHarvestManager.check(flag.pos.roomName);
        }
    }
    
    



    // Rooms
    for(const roomName in Game.rooms) {
        const room = Game.rooms[roomName];
        if(room.controller != undefined) {
            if(room.controller.owner != undefined) {
                if(room.controller.owner.username=="Rhodan" && room.find(FIND_MY_SPAWNS).length>0) {
                    roomManager.staticCheck(roomName);
                    roomManager.check(roomName);
        
                }
    
            } else {
                
            }
        }
    }

    // Creeps

    for(var creepName in Game.creeps) {
        var creep = Game.creeps[creepName];

        try{
            roles["role_"+creep.memory.role].run(creep);
        } catch(e) {
            console.log("Error in Crrep-Run for Creep "+creep.name+" with role "+creep.memory.role);
            console.log("Error message: "+e);
        }
    }

    if(Game.time % 5 == 0) {
        // Population Control
        try {
            population.check();
        } catch (e) {
            console.log("PopulationError: "+e);
        }
    }
    if(Game.time % 5 == 1) {
        // Population Control
        try {
            memoryManager.insertStructures();
        } catch (e) {
            console.log("MemoryError: insertStructures "+e);
        }
    }
    if(Game.time % 5 == 2) {
        // Population Control
        try {
            memoryManager.cleanStructures();
        } catch (e) {
            console.log("MemoryError: cleanStructures "+e);
        }
    }
    if(Game.time % 5 == 3) {
        // Population Control
        try {
            memoryManager.cleanRootMemory();
        } catch (e) {
            console.log("MemoryError: cleanRootMemory "+e);
        }
    }
    
    
    if(Game.time % 10 == 0) {
        // Clean Memory
        for(const creepName in Memory.creeps) {
            if(!Game.creeps[creepName]) {
                delete Memory.creeps[creepName];
                console.log('Clearing non-existing creep memory:', creepName);
            }
        }
    }
    if(Game.time % 30 == 0) {

    }
    if(Game.time % 60 == 0) {
        // memoryManager.insertStructures();
    }
    //  @ts-ignore
    var func = function() {
        // memoryManager.buildRooms()
    };
    // PROFILER--comment in
    // });
};