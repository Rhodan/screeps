/**
 * Created by Rhodan on 29.11.2016.
 */
var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleTowerSupplier = {
    run: function(creep) {
        if(creep.memory.empty==undefined||(creep.memory.empty==false&&creep.carry.energy==0)) {
            creep.memory.empty=true
        }
        if(!creep.memory.empty) {
            var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                }
            });
            if(targets.length > 0) {
                var nearest = creep.pos.findClosestByRange(targets);
                var result = creep.transfer(nearest, RESOURCE_ENERGY)
                if(result == ERR_NOT_IN_RANGE) {
                    creep.moveTo(nearest);
                }
            }

        } else {
            var target = undefined
            if(creep.room.storage.store[RESOURCE_ENERGY]>0) {
                target = creep.room.storage
                var result = creep.withdraw(target, RESOURCE_ENERGY)
                if(result == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                } else if(result==OK) {
                    creep.memory.empty=false
                }
            }
            var result = creep.withdraw(target, RESOURCE_ENERGY)
            if(result == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            } else if(result==OK) {
                creep.memory.empty=false
            }
        }
    }
}
module.exports =roleTowerSupplier
