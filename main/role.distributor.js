/**
 * Created by Rhodan on 29.11.2016.
 */
var util = require('utility');
var utilCreeps = require("utility.creeps");

var roleDistributor = {
    run: function (creep) {
        var target = creep.memory.target;
        var storage = creep.room.storage;
        var fromStorage = storage.store[RESOURCE_ENERGY]>1000;
        if(creep.ticksToLive<40){
            if(creep.transfer(storage,RESOURCE_ENERGY)== ERR_NOT_IN_RANGE) {
                creep.moveTo(storage)
            }
            if(creep.energy==0) {
                creep.suicide()
            }
        }
        if(fromStorage) {
            // Pickup Energy from Storage while walking by
            if(creep.store.getFreeCapacity()>0) {
                creep.withdraw(storage,RESOURCE_ENERGY)
            }
            
            if(creep.store[RESOURCE_ENERGY]>0) {
                // has Energy
                if(target!=null) {
                    
                }
                // creep.transfer(cree)
                // find
                var target = creep.pos.findClosestByPath(FIND_STRUCTURES,{filter: (structure) => {
                    return  (structure.structureType==STRUCTURE_EXTENSION ||
                            structure.structureType==STRUCTURE_SPAWN) &&
                            structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
                }})
                if(target==null) {
                    target = creep.pos.findClosestByRange(FIND_STRUCTURES,{filter: (structure) => {
                    return  structure.structureType==STRUCTURE_TOWER &&
                            structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
                }})
                }
                creep.moveTo(target)
                creep.transfer(target,RESOURCE_ENERGY)
            } else  {
                // is Empty
                creep.moveTo(storage)
                // util.collectAllFromStorage(creep)
                
            }
            
            
        } else {
            
        if(creep.store.getFreeCapacity()>0) {
            for(var spawn of creep.pos.findInRange(FIND_MY_SPAWNS,1,(spawn) => {
                return spawn.store.getUsedCapacity(RESOURCE_ENERGY)>0
            })) {
                creep.withdraw(spawn,RESOURCE_ENERGY)
            }
        }
        if(creep.store[RESOURCE_ENERGY]>0) {
            var target = null
            var target = creep.pos.findClosestByPath(FIND_STRUCTURES,{filter: (structure) => {
                return  structure.structureType==STRUCTURE_EXTENSION &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            }})
            if(target==null) {
                target = creep.pos.findClosestByRange(FIND_STRUCTURES,{filter: (structure) => {
                return  structure.structureType==STRUCTURE_TOWER &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            }})
            }
            if(creep.transfer(target,RESOURCE_ENERGY)== ERR_NOT_IN_RANGE) {
                creep.moveTo(target)
            }
        } else {
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_STORAGE ||
                            (structure.structureType == STRUCTURE_SPAWN && 
                            structure.store[RESOURCE_ENERGY]>50 &&
                            creep.room.storage[RESOURCE_ENERGY]<1000));
                    }
                });
                if(targets.length > 0) {
                    var nearest = creep.pos.findClosestByRange(targets);
                    if(creep.withdraw(nearest, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(nearest);
                    }
                }
            
        }
        }
    },
    /**
     * 
     * @param {StructureSpawn} spawn 
     */
    build: function(spawn) {
        if(spawn.room.energyAvailable>=600) {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined,
                    {role: 'distributor', empty: true, sleep: false, target: null});
        } else if (spawn.room.energyAvailable>=300) {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,MOVE,MOVE], undefined,
                    {role: 'distributor', empty: true, sleep: false, target: null});
        } else {
            spawn.spawnCreep([CARRY,MOVE],"distributor_"+spawn.roomName+"_"+Game.time, { memory : 
                {role: 'distributor', empty: true, sleep: false, target: null}});
        }
        
    }

}
module.exports = roleDistributor