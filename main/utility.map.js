//  @ts-check
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('utility.map');
 * mod.thing == 'a thing'; //  true
 */
var roomScoutManager = require('./roomScoutManager');
var utilityMap = {
    findExternalSources: function() {
        var result = [];
        for(var fl in Game.flags) {
            var flag = Game.flags[fl]; 
            if(flag.name.includes("_HARVESTING") && Game.rooms[flag.pos.roomName].find(FIND_MY_SPAWNS).length<1) {
                
                result.concat(Game.rooms[flag.pos.roomName].find(FIND_SOURCES)) ;
                // console.log(result)
            }
        }
        return result;
    },
    /**
     * Scouts the rooms adjacent to the given room
     * @param {String} roomName 
     */
    scoutAdjacentRooms: function(roomName) {
        // scout adjacent rooms
        const nextRooms = Game.map.describeExits(roomName);
        for(const orientation in nextRooms) {
            const nextRoom = nextRooms[orientation];
            const flags = this.getFlagsForRoom(nextRoom);
            if(!flags.includes("ACTIVE")) roomScoutManager.check(nextRoom);
        }
    },
    /**
     * Get the flags associated with a given room
     * @param {String} roomName 
     * @returns {String[]} names of the strategic Flags for this room  like ["RESERVING","PARK"...]
     */
    getFlagsForRoom: function(roomName) {
        /** @type {String[]} */
        let result = [];
        for (const flagName in Game.flags) {
            const flag = Game.flags[flagName];
            const flagNameArray = flag.name.split("_");
            if(flagNameArray[0] == roomName && flagNameArray.length==2) {
                result.push(flagNameArray[1]);
            }
        }
        return result;

    },

};
module.exports = utilityMap;