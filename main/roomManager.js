//  @ts-check
const roomScoutManager = require('./roomScoutManager');

// @ts-check
var _ = require('lodash');
/**
 * Module for managine a room with spawn and controller
 * @module roomManager
 */
var roomManager = {
    /**
     * Manage a dynamic room (creeps etc.)
     * @param {Room} room 
     */
    check: function(roomName) {
    },
    /**
     * manage structures and memory of a room
     * @param {Room} room 
     */
    staticCheck: function(roomName) {
        // main status check
        console.log("Managing "+roomName);
        const room = Game.rooms[roomName];
        // check own room memory
        let roomMemory = Memory.rooms[room.name];
        if(roomMemory == undefined) {
            roomMemory = Memory.rooms[roomName] = {};
        }
        roomMemory.name = room.name;
        // check adjacent rooms
        const nextRooms = Game.map.describeExits(room.name);
        for(const orientation in nextRooms) {
            const nextRoom = nextRooms[orientation];
            let flagFound = false;
            for (const flagName in Game.flags) {
                const flag = Game.flags[flagName];
                if(flagName == nextRoom+"_SCOUT") {
                    flagFound=true;
                }
            }
            if(!flagFound) new RoomPosition(49,49,nextRoom).createFlag(nextRoom+"_SCOUT");
            roomScoutManager.check(nextRoom);
        }
        // Manage Towers
        /** @type {StructureTower[]} */
        const towers = _.filter(room.getActiveStructures(),{structureType:STRUCTURE_TOWER});
        for(const tower of towers) {
            const closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if(closestHostile==null) {
                  const closestDamagedStructure  = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                    /** @param structure {Structure} */
                    filter: (structure) => structure.hits < structure.hitsMax &&
                        (structure.hits<50000 ||
                        (structure.structureType==STRUCTURE_STORAGE && structure.hits<200000))
                });
                if(closestDamagedStructure&&tower.energy>400) {
                    tower.repair(closestDamagedStructure);
                }
            } else {
                console.log(tower);
                tower.attack(closestHostile);
            }
        }

        

        // Manage Links
        /** @type {StructureLink[]} */
        const links = _.filter(room.getActiveStructures(),{structureType:STRUCTURE_LINK});
        // check if there are links
        if(links.length>0) {
            // if yes manage link logic
            room.labelLinks(links);
            let linkStorage;
            let linkSources = [];
            for(const link of links) {
                if(link.memory.role=="linkStorage") linkStorage=link;
                if(link.memory.role=="linkSource") linkSources.push(link);
            }
            
            for(const link of linkSources) {
                if(link.cooldown == 0 && link.store[RESOURCE_ENERGY]>100) {
                    link.transferEnergy(linkStorage);
    
                }
            }
        }
        // manage containers
        /** @type {StructureContainer[]} */
        const containers = room.getContainers();
        // check if there are containers
        room.labelContainers(room.getContainers());
        for (const container of containers) {
            if (container.memory.role == "containerController") {
                container.memory.haulers = container.memory.haulers || [];
                if (container.memory.haulers.length < 1) {
                    /** @type {StructureSpawn} */
                    const spawn = room.find(FIND_MY_SPAWNS)[0];
                    /** @type {StructureStorage} */
                    const storage = room.storage;
                    console.log(storage);
                    let result = spawn.spawnCreep([CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE],
                        "containerHauler_" + container.id + "_" + Game.time, { memory: { origin: storage.id, destination: container.id, role: "containerHauler" } });
                    if (result == OK) {
                    }
                }
            }
        }

        // manage spawns
        /** @type {Spawn[]} */
        const spawns = room.find(FIND_MY_SPAWNS);
        for (const spawn of spawns) {
            let creeps = spawn.pos.findInRange(FIND_MY_CREEPS,1,{
                /** @param creep {Creep} */
                filter: (creep) => {
                    return creep.memory.role != "commandSupplier" && creep.ticksToLive<1200;
            }});
            if (room.energyAvailable>300) {
                spawn.renewCreep(creeps[0]);

            }
        }

        /** @type {Source[]} */
        let sources = room.find(FIND_SOURCES);
        for (const source of sources) {
            source.memory.miners = source.memory.miners || [];
            
        }



    },
    
};
module.exports = roomManager;

