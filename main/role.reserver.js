var util = require("utility");
var utilCreeps = require("utility.creeps");

const STATE_SPAWNING = 0;
const STATE_MOVING = 1;
const STATE_RESERVING = 2;
var roleReserver = {

    /** 
     * genral run function
     * @param {Creep} creep 
     */
    run: function(creep) {
        // check if state is defined
        if(!creep.memory.state) {
            // if not, set spawning as current state
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;
            case STATE_MOVING:
                this.runMoving(creep);
                break;
            case STATE_RESERVING:
                this.runReserving(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if initialized
        if(!creep.memory.init) {
            // if no, init this creep
            creep.memory.dyingWish = false;
            Game.rooms[creep.memory.roomTarget].memory.reserver.push(creep.name);
            
            creep.memory.init = true;
        }
        // check if creep is still spawning
        if(!creep.spawning) {
            // if no, change to next state and run it
            creep.memory.state = STATE_MOVING;
            this.run(creep);
        }
    },
    /**
     * function while moving to a room
     * @param {Creep} creep 
     */
    runMoving: function(creep) {
        const result = creep.moveTo(new RoomPosition(25, 25, creep.memory.roomTarget));
        // check if creep is still spawning
        if(creep.room.name == creep.memory.roomTarget) {
            // if no, change to next state and run it
            creep.memory.state = STATE_RESERVING;
            this.run(creep);
        }
    },
    /**
     * function while moving to a room
     * @param {Creep} creep 
     */
    runReserving: function(creep) {
        const room = Game.rooms[creep.memory.roomTarget];
        const controller = room.controller;
        if(creep.reserveController(controller) == ERR_NOT_IN_RANGE) {
            creep.moveTo(controller);
        }
        if(creep.ticksToLive<50 && !creep.memory.dyingWish) {
            /** @type {String[]} */
            let reserver = creep.memory.reserver;
            creep.memory.dyingWish = true;
            reserver.splice(reserver.indexOf(creep.name),1);
            creep.room.memory.reserving=false;
        }
    },
};
module.exports = roleReserver;