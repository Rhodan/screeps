/**
 * Created by Rhodan on 28.11.2016.
 */
var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleHauler = {
    run: function (creep) {
        if (creep.carry.energy == 0) {
            var targets = undefined
            var drops = creep.room.find(FIND_DROPPED_RESOURCES, {
                filter: (resource) => {
                    // console.log(resource)
                    // console.log(resource.resourceType==RESOURCE_ENERGY&&resource.amount>=creep.carryCapacity)
                    return resource.resourceType==RESOURCE_ENERGY&&resource.amount>=creep.carryCapacity
                }
                
            })
            // console.log(drops)
            if(drops.length>0) {
                targets = drops
                var nearest = creep.pos.findClosestByRange(targets);
                if (creep.pickup(nearest) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(nearest);
                }

            } else {
                targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER);
                    }
                });
                if (targets.length > 0) {
                    var nearest = creep.pos.findClosestByRange(targets);
                    if (creep.withdraw(nearest, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(nearest);
                    }
                }
            }


        } else {
            var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_STORAGE ||
                        structure.structureType== STRUCTURE_EXTENSION) && structure.energy < structure.energyCapacity;
                }
            });
            if (targets.length > 0) {
                var nearest = creep.pos.findClosestByRange(targets);
                if (creep.transfer(nearest, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(nearest);
                }
            } else {
                if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.storage);
                }
            }
        }
    },
    build: function(spawn) {
        if(spawn.room.energyCapacityAvailable>450) {
            
        }
        spawn.createCreep([CARRY,CARRY,MOVE,MOVE], undefined, {role: 'hauler'})
    }
}
module.exports = roleHauler
