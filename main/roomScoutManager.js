// @ts-check
var _ = require('lodash');
var util = require('./utility');
/**
 * Module for managing a room for scouting
 * @module roomScoutManager
 */
var roomScoutManager = {
    /**
     * Manage a given room as a scouting target
     * @param {String} roomName 
     */
    check: function(roomName) {

        console.log("Scouting "+roomName);

        /** @type {Room} */
        const room = Game.rooms[roomName];
        /** @type {Object} */
        let roomMemory = Memory.rooms[roomName];
        // check if room Memory has already been initialized
        if(roomMemory == undefined) {
            // if no, initialize the room memory
            // @ts-ignore
            roomMemory = Memory.rooms[roomName] = {};  
        }
        roomMemory.name = roomMemory.name || roomName;
        roomMemory.lastScout = roomMemory.lastScout || Game.time-1500;
        roomMemory.scouts = roomMemory.scouts || [];
        // check if there is vision on this room
        // console.log("Time to next scout "+ (roomMemory.lastScout+1400-Game.time)+" with "+ (Game.time > roomMemory.lastScout+1400));
        this.cleanUp(roomName);
        if (roomMemory.scouts.length<1) {
            // if no, build a scout creep
            this.buildScoutingCreep(roomName);
        }
        


    },
    /**
     * build a scout-creep for this room
     * @param {String} roomName 
     */
    buildScoutingCreep: function(roomName) {
        let roomMemory = Memory.rooms[roomName];

        const spawnObject = util.findNearestSpawn(new RoomPosition(25, 25, roomName));
        let spawn = Game.getObjectById(spawnObject.id);
        // console.log(spawn+"/"+spawnObject.spawnId);
        let result = spawn.spawnCreep([MOVE],"scout_"+roomName+"_"+Game.time,{memory: {roomTarget: roomName,role:"scout"}});
        if(result == OK) {
            console.log("build scout for "+roomName);
            roomMemory.lastScout = Game.time;
        }
    },
    /**
     * 
     * @param {string} room 
     */
    cleanUp: function (roomName) {
        if (Game.time % 10 == 4) {
            /** @type {String[]} */
            const scouts = Memory.rooms[roomName].scouts;
            for (const creepName of scouts) {
                if (Game.creeps[creepName] == undefined) {
                    scouts.splice(scouts.indexOf(creepName), 1);
                }
            }
        }
    },
};
module.exports = roomScoutManager;