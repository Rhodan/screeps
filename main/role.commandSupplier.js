var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleCommandSupplier = {

    /**
     * 
     *  @param {Creep} creep 
     */
    run: function(creep) {
        
        if(creep.memory.supplying && creep.carry.energy == 0) {
            creep.memory.supplying = false;
	    }
	    if(!creep.memory.supplying && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.supplying = true;
	    }

	    if(creep.memory.supplying) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        }
        else {
            const container = creep.room.getControllerContainer();
            if (container != undefined) {
                if (creep.withdraw(container,RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(container);
                }
            } else {

                util.collectEnergyStored(creep,creep.carryCapacity)
            }
        }
    },
    /**
     * 
     * @param {*} spawn 
     */
	build: function(spawn) {
	    if(spawn.room.energyCapacityAvailable>600) {
            spawn.createCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined,
	                {role: 'commandSupplier', empty: true, sleep: false, supplying: false})
        } else {
	        spawn.createCreep([WORK,CARRY,CARRY,MOVE,MOVE], undefined,
	                    {role: 'commandSupplier', empty: true, sleep: false, supplying: false})
            
        }
	}
};

module.exports = roleCommandSupplier;