var spawnExtension = function() {
    StructureSpawn.prototype.generateCreepTemplateToCeiling= function(template,ceiling,energy) {
        var levelCost = 0;
        // calculate cost for one iteration of the template
        for(var part of template) {
            levelCost += BODYPART_COST[part];
        }
        
        // calculate how big the creep can get before hitting energy or ceiling cap
        var maxLevel = Math.floor(Math.min(ceiling,energy)/levelCost);
        
        // build template to the calculated level
        var result = template;
        for(var i = 0;i<maxLevel-1;i++) {
            result = result.concat(template);
        }
        return result;
    };
    StructureSpawn.prototype.spawnCreepFromTemplate= function(room,template,mem) {
        // Find non-busy spawns
        var spawns = room.find(FIND_MY_SPAWNS,{filter: (spawn) => {
            return spawn.spawning==null;
        }})
        
        spawns[0].spawnCreep(mem.role+"_"+Game.time,template,mem);
        
    };
}
module.exports = spawnExtension;