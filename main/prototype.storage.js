var storageExtension = function() {
    StructureStorage.prototype.getTopResource = function() {
        var key;
        var amount = 0;
        for(var k in this.store) {
            // console.log(k+"/"+this.store[k]);
            if(this.store[k]>amount) {
                key = k;
                amount = this.store[k];
            }
        }
        return key;
    };
};
module.exports = storageExtension;