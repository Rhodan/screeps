/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('codeExtension');
 * mod.thing == 'a thing'; //  true
 */
var codeExtension = {
    extendPrototypes: function() {
        // Extend sources with memory
        Object.defineProperty(Source.prototype, 'memory', {
            configurable: true,
            get: function() {
                if(_.isUndefined(Memory.sources)) {
                    Memory.sources = {};
                }
                if(!_.isObject(Memory.sources)) {
                    return undefined;
                }
                return Memory.sources[this.id] = Memory.sources[this.id] || {};
            },
            set: function(value) {
                if(_.isUndefined(Memory.sources)) {
                    Memory.sources = {};
                }
                if(!_.isObject(Memory.sources)) {
                    throw new Error('Could not set source memory');
                }
                Memory.sources[this.id] = value;
            }
        });
        
        // Extend structures with reserve
        Object.defineProperty(Structure.prototype, 'reserved', {
            configurable: true,
            get: function() {
                if(_.isUndefined(Memory.structures[this.id])) {
                    Memory.structures[this.id] = {};
                }
                if(_.isUndefined(Memory.structures[this.id].reserved)) {
                    Memory.structures[this.id].reserved = {};
                }
                if(!_.isObject(Memory.structures[this.id].reserved)) {
                    return undefined;
                }
                return Memory.structures[this.id].reserved = Memory.structures[this.id].reserved || {};
            },
            set: function(value) {
                if(_.isUndefined(Memory.structures[this.id].reserved)) {
                    Memory.structures[this.id].reserved = {};
                }
                if(!_.isObject(Memory.structures[this.id].reserved)) {
                    throw new Error('Could not set Reserved memory');
                }
                Memory.structures[this.id].reserved = value;
            }
        });
        
    },
};
module.exports = codeExtension;