var roleEnergySupplier = {

    /** @param {Creep} creep **/
    run: function(creep) {

        if(creep.memory.supplying && creep.carry.energy == 0) {
            creep.memory.supplying = false;
	    }
	    if(!creep.memory.supplying && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.supplying = true;
	    }

	    if(creep.memory.upgrading) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        }
        else {
            var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0]);
            }
        }
	}
};

module.exports = roleEnergySupplier;