/**
 * Created by Rhodan on 28.11.2016.
 */
var util = require("./utility");
var utilCreeps = require("./utility.creeps");

const STATE_SPAWNING = 0;
const STATE_SEARCHING_ORIGIN = 1;
const STATE_COLLECTING = 2;
const STATE_SEARCHING_DESTINATION = 3;
const STATE_DEPOSITING = 4;

var roleContainerHauler = {
    genericTemplate: [CARRY,CARRY,MOVE],
    /** 
     * genral run function
     * @param {Creep} creep 
     */
    run: function(creep) {
        // if no state defined => was just created 
        if(!creep.memory.state) {
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;
            case STATE_SEARCHING_ORIGIN:
                this.runSearchingOrigin(creep);
                break;
            case STATE_COLLECTING:
                this.runCollecting(creep);
                break;
            case STATE_SEARCHING_DESTINATION:
                this.runSearchingDestination(creep);
                break;
            case STATE_DEPOSITING:
                this.runDepositing(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if creep has been initialized
        if(!creep.memory.init) {
            // if no, run init code
            if (creep.memory.origin != undefined) {
                let origin = Game.getObjectById(creep.memory.origin);
                origin.memory.haulers.push(creep.name);
            }
            creep.memory.init = true;
        }
        if(!creep.spawning) {
            creep.memory.state = STATE_SEARCHING_ORIGIN;
            this.run(creep); 
        }
    },
    /**
     * function while searching for origin of resources
     * @param {Creep} creep 
     */
    runSearchingOrigin: function(creep) {
        let hasFoundTarget = false;
        // Check if creep has constant origin
        if (creep.memory.origin == undefined) {
            // if not, find first links then containers with resources
            let target;
            /** @type {StructureLink[]} */
            const links = _.filter(creep.room.getLinks(), 
                /** @param link {StructureLink} */
                (link) => {
                    return link.memory.role == "linkStorage" && link.store[RESOURCE_ENERGY]>creep.carryCapacity;
                });
            if(links.length>0) {
                target = links[0];
                creep.memory.target = target.id;
                target.memory.reserved += creep.carryCapacity;
                hasFoundTarget = true;
            }
            if (target==undefined) {
                /** @type {StructureContainer[]} */
                let containers = _.filter(creep.room.getContainers(),
                    /** @param container {StructureContainer} */
                    (container) => {
                        return (container.memory.role == "containerSource" ||container.memory.role == "containerMineral")&&
                            container.store[container.getTopResource()] - container.memory.reserved > creep.carryCapacity;
                    });
                // check if there are any containers with enough resources
                if (containers.length > 0) {
                    // if yes, set closest container as target and reserve resources
                    /** @type {StructureContainer} */
                    target = creep.pos.findClosestByRange(containers);
                    creep.memory.target = target.id;
                    target.memory.reserved += creep.carryCapacity;
                    hasFoundTarget = true;
                }
            }

        } else {
            creep.memory.target = creep.memory.origin;
            hasFoundTarget = true;
        }
        if(hasFoundTarget) {
            creep.memory.state = STATE_COLLECTING;
            this.run(creep);
        } else {
            creep.moveTo(Game.flags[creep.room.name+"_DIST"].pos)
        }
            
    },
    /**
     * function while collecting resources from origin
     * @param {Creep} creep 
     */
    runCollecting: function(creep) {
        // move to target and withdraw energy
        /** @type {StructureContainer} */
        let target = Game.getObjectById(creep.memory.target);
        let resource = target.getTopResource();
        // check if dying
        if(creep.ticksToLive<util.findNearestStorage(target.pos).distance*2.5) {
            
            // free Reserved Energy and delete Target
            target.memory.reserved -= creep.carryCapacity;
            creep.memory.target=undefined;
            creep.suicide();
        }
        
        // try to withdraw energy, move to target if out of range
        const result = creep.withdraw(target, resource);
        if (result  != OK) {
            creep.moveTo(target);
        }

        // check if creep has resources in storage
        if(creep.store.getUsedCapacity()>0) {
            // if so transition to depositing state
            target.memory.reserved -= creep.store.getUsedCapacity();
            creep.memory.target = undefined;
            creep.memory.state = STATE_SEARCHING_DESTINATION;
            this.run(creep);
        }
    },
    /**
     * function searchin for destination of resources
     * @param {Creep} creep 
     */
    runSearchingDestination: function(creep) {
        // check if creep has fixed destination
        if(!creep.memory.destination) {
            // if no, find target structure

            // check if room has storage
            if(!creep.room.storage && creep.store[RESOURCE_ENERGY]>0) {
                // if no, search for non-full spawns´
                /** @type {StructureSpawn[]} */
                let spawns = creep.room.find(FIND_MY_SPAWNS);
                /** @type {StructureSpawn[]} */
                let targets = creep.room.find(spawns, {
                    filter: (structure) => {
                        return structure.store[RESOURCE_ENERGY] < structure.store.getCapacity();
                    }
                });
                // check if there are non-full spawns
                if (targets.length > 0) {
                    // if yes,target nearest 
                    creep.memory.target = creep.pos.findClosestByRange(targets)
                } else {
                    // if no, go to nearst spawn
                    creep.memory.target = creep.pos.findClosestByRange(spawns);
                }
            } else {
                // if yes, go to storage
                creep.memory.target=creep.room.storage.id;
            }
        } else {
            // if yes, go to destination
            let target = Game.getObjectById(creep.memory.destination);
            if (target == undefined) {
                creep.memory.destination = undefined;
                creep.memory.source = undefined;
            }
            creep.memory.target = creep.memory.destination;
        }
        creep.memory.state = STATE_DEPOSITING;
        this.run(creep);
        
    },
    /**
     * function while depositing resources into destination
     * @param {Creep} creep 
     */
    runDepositing: function(creep) {
        /** @type {Structure} */
        let target = Game.getObjectById(creep.memory.target);
        let resource = creep.getTopResource();
        if(creep.transfer(target, resource) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }




        // check if creep is empty
        if(creep.store.getUsedCapacity()==0) {
            // if yes, transition to collecting state
            creep.memory.target = undefined;
            creep.memory.state = STATE_SEARCHING_ORIGIN;
            this.run(creep);
        }
    },
    
        // Check if Creep is empty or is full
        
    
    build: function(spawn) {
        if(spawn.room.energyCapacityAvailable>600) {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined, {role: 'containerHauler'})
        } else {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,MOVE,MOVE], undefined, {role: 'containerHauler'})
        }
    }
}
module.exports = roleContainerHauler;
