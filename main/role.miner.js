
//  @ts-check
var _ = require('lodash');
var util = require("./utility");
var utilCreeps = require("./utility.creeps");

const STATE_SPAWNING = 0;
const STATE_SEARCHING = 1;
const STATE_WALKING = 2;
const STATE_MINING = 3;
var roleMiner = {

    /**
     * general run function
     *  @param {Creep} creep
     */
    run: function(creep) {
        if(!creep.memory.state) {
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;
            case STATE_SEARCHING:
                this.runSearching(creep);
                break;
            case STATE_WALKING:
                this.runWalking(creep);
                break;
            case STATE_MINING:
                this.runMining(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if initialized
        let target = Game.getObjectById(creep.memory.target);
        if(!creep.memory.init) {
            // if no, init this creep
            creep.memory.dyingWish = false;
            if(target != undefined) target.memory.miners.push(creep.name);
            creep.memory.init = true;
        }
        // check if creep has spawned
        if(!creep.spawning) {
            creep.memory.state = STATE_SEARCHING;
            this.run(creep);
        }
    },
    /**
     * function while searching for resoures to harvest
     * @param {Creep} creep 
     */
    runSearching: function(creep) {
        // check if target is already defined
        if(creep.memory.target == undefined) {
            // if no, find unoccupied source
            /** @type {Source} */
            let target;
            /** @type {Source[]} */
            const sources = creep.room.find(FIND_SOURCES)
            for(const source of sources) {
                var occupied = false;
               
                const miners = _.filter(Game.creeps, 
                    (cr) => {
                        return cr.room.name == creep.room.name && cr.memory.role == "miner";
                    });
                for(var c of miners) {
                    if(c.memory.target==source.id) {occupied=true;}
                }
                
                if(!occupied){
                    target = source;
                    creep.memory.target = target.id;
                    target.memory.miners.push(creep.name);
                     break;
                }
               
            }
            
        }
        // check if creep has target
        if(creep.memory.target != undefined) {
            // if yes, transition to walking state
            creep.memory.state = STATE_WALKING;
            this.run(creep);
        }
    },
    /**
     * function while walking to target
     * @param {Creep} creep 
     */
    runWalking: function(creep) {
        /** @type {Source} */
        let target = Game.getObjectById(creep.memory.target);
        creep.moveTo(target);
        if(creep.pos.inRangeTo(target,1)) {
            creep.memory.state = STATE_MINING;
            this.run(creep);
        }
    },
    /**
     * function while harvesting resources
     * @param {Creep} creep 
     */
    runMining: function(creep) {
        /** @type {Source} */
        let target = Game.getObjectById(creep.memory.target);
        creep.harvest(target);
        if(creep.ticksToLive<50 && !creep.memory.dyingWish) {
            /** @type {StructureSpawn} */
            let spawn = Game.getObjectById(util.findNearestSpawn(creep.pos).id);
            // @ts-ignore
            let result = spawn.spawnCreep(this.getTemplate(spawn.room.energyAvailable),"miner_"+target.id+"_"+Game.time,{memory: {target: target.id,role:"miner"}});
            if(result == OK) {
                creep.memory.dyingWish=true;
            }
        }
        // check if creep is dying
        if(creep.ticksToLive==1) {
            //  if yes
            /** @type {String[]} */
            let sourceMiners = Memory.sources[creep.memory.target].miners;
            sourceMiners.splice(sourceMiners.indexOf(creep.name),1);
        }
        let droppedResources = creep.pos.lookFor(LOOK_RESOURCES);
        if (droppedResources.length>0) {
            creep.pickup(droppedResources[0]);
        }
        if(creep.store[RESOURCE_ENERGY]>0) {
            // check if creep has a link defined
            if(creep.memory.link != undefined) {
                // if yes, transfer energy to link
                /** @type {StructureLink} */
                let link = Game.getObjectById(creep.memory.link);
                if(creep.transfer(link,RESOURCE_ENERGY) == ERR_FULL) {
                    creep.drop(RESOURCE_ENERGY);
                }
                return "deposited energy into link";
            } else {
                // if no, try to find link
                /** @type {StructureLink[]} */
                let links = creep.pos.findInRange(FIND_STRUCTURES, 1, {filter: (structure) => {
                    return structure.structureType == STRUCTURE_LINK;
                }});
                
                if (links.length > 0) {
                    creep.memory.link = links[0].id;
                } 
            }
            // check if creep has a container defined
            if(creep.memory.container != undefined) {
                // if yes, transfer energy to container
                /** @type {StructureContainer} */
                let container = Game.getObjectById(creep.memory.container);
                // check if container is damaged
                if(container.hits<200000) {
                    // if yes, repair it
                     creep.repair(container);
                     return "repaired container";
                } else {
                    // if no, transfer energy into container
                    if(creep.transfer(container,RESOURCE_ENERGY) == ERR_FULL) {
                        creep.drop(RESOURCE_ENERGY);
                    }
                    return "deposited energy into container";
                }
            } else  {
                // if no, try to find container
                /** @type {StructureContainer[]} */
                let containers = creep.pos.findInRange(FIND_STRUCTURES, 1, {filter: (structure) => {
                    return structure.structureType == STRUCTURE_CONTAINER;
                }});
                if (containers.length > 0) {
                    creep.memory.container = containers[0].id;
                } 
            }
            creep.drop(RESOURCE_ENERGY);
            return "dropped energy";
        }
    },
	build: function(spawn) { 
        if(spawn.room.energyCapacityAvailable>=950){spawn.createCreep([WORK,WORK,WORK,WORK,WORK,WORK,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE], undefined, {role: 'miner'})}
	    else if(spawn.room.energyCapacityAvailable>=700){spawn.createCreep([WORK,WORK,WORK,WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'miner'})}
	    else if(spawn.room.energyCapacityAvailable>=450){spawn.createCreep([WORK,WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'miner'})}
	    else if(spawn.room.energyCapacityAvailable>=350){spawn.createCreep([WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'miner'})}
	    else if(spawn.room.energyCapacityAvailable>=250){spawn.createCreep([WORK,WORK,CARRY,MOVE], undefined, {role: 'miner'})}
    },
    /**
     * 
     * @param {Number} energy 
     */
    getTemplate: function(energy) {
        let template;
        if(energy>=950){template = [WORK,WORK,WORK,WORK,WORK,WORK,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE]}
	    else if(energy>=700){template = [WORK,WORK,WORK,WORK,WORK,WORK,CARRY,MOVE]}
	    else if(energy>=450){template = [WORK,WORK,WORK,WORK,CARRY,MOVE]}
	    else if(energy>=350){template = [WORK,WORK,WORK,CARRY,MOVE]}
	    else if(energy>=250){template = [WORK,WORK,CARRY,MOVE]}
        return template;
    }
};

module.exports = roleMiner;