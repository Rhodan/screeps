// @ts-check
var _ = require('lodash');
var util = require('./utility');

var roleMiner = require('./role.miner');
var roomScoutManager = require('./roomScoutManager');
/**
 * Module for managing a room for harvesting
 * @module roomHarvestManager
 */
var roomHarvestManager = {
    check: function (roomName) {
        console.log("Harvesting " + roomName);
        const room = Game.rooms[roomName];
        let roomMemory = Memory.rooms[roomName];
        if (room == undefined) {
            if (roomMemory == undefined) {
                // @ts-ignore
                roomMemory = Memory.rooms[roomName] = {};
            }
            roomMemory.name = roomMemory.name || roomName;
            return "no vision on room" + roomName;
        }


        /** @type {Source[]} */
        const sources = room.find(FIND_SOURCES);
        for (const source of sources) {
            source.memory.miners = source.memory.miners || [];
            console.log("looking for source "+source.id+"_"+source.memory.miners.length);
            if (source.memory.miners.length == 0) {
                /** @type {StructureSpawn} */
                const spawn = Game.getObjectById(util.findNearestSpawn(source.pos).id);
                let result = spawn.spawnCreep(roleMiner.getTemplate(spawn.room.energyAvailable),
                    "miner_" + source.id + "_" + Game.time, { memory: { target: source.id, role: "miner" } });
                if (result == OK) {

                }
            }
        }
        /** @type {StructureContainer[]} */
        let containers = room.getContainers();
        if (containers.length > 0) {
            // if yes, manage containers
            room.labelContainers(containers);
            for (const container of containers) {
                if (container.memory.role == "containerSource") {
                    container.memory.haulers = container.memory.haulers || [];
                    if (container.memory.haulers.length < 3) {
                        /** @type {StructureSpawn} */
                        const spawn = Game.getObjectById(util.findNearestSpawn(container.pos).id);
                        /** @type {StructureStorage} */
                        const storage = Game.getObjectById(util.findNearestStorage(container.pos).id);
                        console.log(storage);
                        let result = spawn.spawnCreep([CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE],
                            "containerHauler_" + container.id + "_" + Game.time, { memory: { origin: container.id, destination: storage.id, role: "containerHauler" } });
                        if (result == OK) {
                        }
                    }
                }
            }

        }
        this.cleanUp(room);


    },
    /**
     * 
     * @param {Room} room 
     */
    cleanUp: function (room) {

        if (Game.time % 10 == 1) {
            for (const source of room.getSources()) {
                let miners = source.memory.miners;
                /** @type {String[]} */
                for (const creepName of miners) {
                    if (Game.creeps[creepName] == undefined) {
                        miners.splice(miners.indexOf(creepName), 1);
                    }
                }

            }
        }
        if (Game.time % 10 == 2) {
            let containers = room.getContainers();
            for (const container of containers) {
                let haulers = container.memory.haulers || [];
                /** @type {String[]} */
                for (const creepName of haulers) {
                    if (Game.creeps[creepName] == undefined) {
                        haulers.splice(haulers.indexOf(creepName), 1);
                    }
                }

            }
        }
    }
};
module.exports = roomHarvestManager;