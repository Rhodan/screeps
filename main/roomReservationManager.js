// @ts-check
var _ = require('lodash');
var util = require('./utility');
var utilMap = require('./utility.map');

var roomScoutManager = require('./roomScoutManager');

var roomReservationManager = {
    check: function(roomName) {
        console.log("Reserving "+roomName);
        const room = Game.rooms[roomName];
        let roomMemory = Memory.rooms[roomName];
        if(roomMemory == undefined) {
            // @ts-ignore
            roomMemory = Memory.rooms[roomName] = {};
        }
        roomMemory.name = roomMemory.name || roomName;
        roomMemory.reserver = roomMemory.reserver || [];
        utilMap.scoutAdjacentRooms(roomName);
        








        if(room != undefined) {
            let reservation = room.controller.reservation;
            if (!reservation) reservation = {username: undefined,ticksToEnd:0};
            if(room.memory.reserver.length==0  && reservation.ticksToEnd < 3000) {
                this.buildReservationCreep(roomName); 
            }
            this.cleanUp(room);
        }
            
    },
    buildReservationCreep: function(roomName) {
        const spawnObject = util.findNearestSpawn(new RoomPosition(25, 25, roomName));
                let spawn = Game.getObjectById(spawnObject.id);
                let result = spawn.spawnCreep([CLAIM,CLAIM,CLAIM,MOVE,MOVE,MOVE],"reserver_"+roomName+"_"+Game.time,{memory: {roomTarget: roomName,role:"reserver"}});
                if(result == OK) {
                    Memory.rooms[roomName].reserving=true;
                }
    },
    /** @param {Room} room */
    cleanUp: function(room) {


        if(Game.time % 10 == 3) {
            /** @type {String[]} */
            let reserver = room.memory.reserver;
            for(const creepName of reserver) {
                if(Game.creeps[creepName] == undefined) {
                    
                    reserver.splice(reserver.indexOf(creepName),1);
                }
            }
        }
    }
};
module.exports = roomReservationManager;
