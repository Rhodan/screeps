var util = require("utility");
var utilCreeps = require("utility.creeps");

const STATE_SPAWNING = 0;
var roleTemplate = {

    /** 
     * genral run function
     * @param {Creep} creep 
     */
    run: function(creep) {
        // check if state is defined
        if(!creep.memory.state) {
            // if not, set spawning as current state
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if initialized
        if(!creep.memory.init) {
            // if no, init this creep

            creep.memory.init = true;
        }
        // check if creep is still spawning
        if(!creep.spawning) {
            // if no, change to next state and run it
            creep.memory.state = STATE_SPAWNING;
            this.run(creep);
        }
    },
}
module.exports = roleTemplate;