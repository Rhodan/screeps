var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(!creep.memory.empty && creep.store.getUsedCapacity()==0) {
            creep.memory.empty=true
        }
        if(creep.memory.empty && creep.store.getFreeCapacity()==0) {
            creep.memory.empty=false
        }
        var drops = creep.pos.findInRange(FIND_DROPPED_RESOURCES,1)
        if(drops.length>0) {
            creep.pickup(creep.pos.findClosestByRange(drops))
        }
	    if(creep.memory.empty) {
            var target = Game.getObjectById('5bbcae399099fc012e638995');
            if(creep.harvest(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }
        } else {
            var target = creep.pos.findClosestByPath(FIND_STRUCTURES,{filter: (structure) => {
                return  (structure.structureType==STRUCTURE_EXTENSION ||
                        structure.structureType==STRUCTURE_SPAWN) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            }})
            console.log(target+"/"+target.store.getFreeCapacity(RESOURCE_ENERGY))   
            
            if(creep.room.energyAvailable==creep.room.energyCapacityAvailable) {
                target = creep.room.storage
            }
            creep.moveTo(target)
            creep.transfer(target, RESOURCE_ENERGY)
        }



	},
	build: function(spawn) {
	    if(spawn.room.energyCapacityAvailable>450) {
            
        }
	    spawn.createCreep([WORK,WORK,CARRY,MOVE], undefined,
                    {role: 'harvester', empty: true, sleep: false});
	}
};

module.exports = roleHarvester;