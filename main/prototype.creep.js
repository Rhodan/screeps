var util = require('./utility');

var creepExtension = function() {
    Creep.prototype.getTopResource= function() {
        var key;
        var amount = 0;
        for(var k in this.store) {
            // console.log(k+"/"+this.store[k]);
            if(this.store[k]>amount) {
                key = k;
                amount = this.store[k];
            }
        }
        return key;
    };
    /**
     * @param {String} roomName
     */
    Creep.prototype.moveToRoom = function(roomName) {
        if (this.room.name == roomName) {
            this.moveTo(new RoomPosition(25,25,roomName));
        }
    };
    /**
     * 
     */
    Creep.prototype.moveToHome = function() {
        let target;
        /** @type {String} */
        const home = this.memory.homeRoom;
        if  (home == undefined) {
            target = Game.getObjectById(util.findNearestSpawn(this.pos).id).room.name;
        } else {
            target = home;
        }
        this.moveTo(new RoomPosition(25,25,this.memory.homeRoom));
    };
}
module.exports = creepExtension;