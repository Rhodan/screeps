var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleExtractor = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(typeof creep.memory.target === "undefined") {
            var mineral = creep.room.find(FIND_MINERALS)[0];
            // console.log(JSON.stringify(mineral));
            var extractor = creep.room.lookForAt(LOOK_STRUCTURES,mineral)[0];
            if(extractor != undefined) {
                creep.memory.target = mineral.id;
            } else {
                console.log("Cant find Mineral target");
            }
            // console.log(JSON.stringify(extractor));
                //  var occupied = false
                //  for(var c of _.filter(Game.creeps, (cr) => cr.room.name == creep.room.name)) {
                //      console.log(c)
                //      if(c.memory.role=="extractor" && c.memory.target==s.id) {occupied=true}
                //  }
                
                //  if(!occupied){
                //      console.log("no creep for this source found")
                //      console.log("insert"+s.id+"into creep"+creep.name)
                //       break;
                //  }
               
            
            
        }
        var target = Game.getObjectById(creep.memory.target);
        // console.log(JSON.stringify(target));
	    if(creep.store.getFreeCapacity()>0) {
            // console.log("should be walking")
            // console.log(creep.harvest(target))
            if(creep.harvest(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }
        }
        if(creep.store.getUsedCapacity()>0) {
            let resource = creep.getTopResource();
            var container = creep.pos.findInRange(FIND_STRUCTURES,0,{filter: (structure) => {
                return structure.structureType==STRUCTURE_CONTAINER;}})[0];
            if(container != undefined) {
                creep.transfer(container,resource);
            } else {
                creep.drop(resource);

            }
            
        }
	},
	build: function(spawn) {
        if(spawn.room.energyCapacityAvailable>=700){
            spawn.createCreep([WORK,WORK,WORK,WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'extractor'});
        } else if(spawn.room.energyCapacityAvailable>=450){
            spawn.createCreep([WORK,WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'extractor'});
        } else if(spawn.room.energyCapacityAvailable>=350) {
            spawn.createCreep([WORK,WORK,WORK,CARRY,MOVE], undefined, {role: 'extractor'});
        } else if(spawn.room.energyCapacityAvailable>=250){
            spawn.createCreep([WORK,WORK,CARRY,MOVE], undefined, {role: 'extractor'});
        }
	}
};

module.exports = roleExtractor;