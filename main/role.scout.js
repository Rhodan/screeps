var util = require("utility");
var utilCreeps = require("utility.creeps");

const STATE_SPAWNING = 0;
const STATE_MOVING = 1;
const STATE_OBSERVING = 2;

var roleScout = {

    /** 
     * genral run function
     * @param {Creep} creep 
     */
    run: function(creep) {
        // check if state is defined
        if(!creep.memory.state) {
            // if not, set spawning as current state
            
            creep.memory.state=STATE_SPAWNING;
        }
        // sleeping
        if(creep.memory.sleep) return null;

        // switch states
        switch (creep.memory.state) {
            case STATE_SPAWNING:
                this.runSpawning(creep);
                break;
            case STATE_MOVING:
                this.runMoving(creep);
                break;
            case STATE_OBSERVING:
                this.runObserving(creep);
                break;

        }
    },
    /**
     * function while spawning
     * @param {Creep} creep 
     */
    runSpawning: function(creep) {
        // check if initialized
        if(!creep.memory.init) {
            // if no, init this creep
            const targetRoomMemory = Memory.rooms[creep.memory.roomTarget];
            console.log(targetRoomMemory.scouts.push(creep.name));
            console.log(targetRoomMemory.scouts);
            creep.memory.init = true;
        }
        // check if creep is still spawning
        if(!creep.spawning) {
            // if no, change to next state and run it
            creep.memory.state = STATE_MOVING;
            this.run(creep);
        }
    },
    /**
     * function while moving to a room
     * @param {Creep} creep 
     */
    runMoving: function(creep) {
        const result = creep.moveTo(new RoomPosition(25, 25, creep.memory.roomTarget));
        const room = creep.room;
        // check if creep is still spawning
        if(room.name == creep.memory.roomTarget) {
            // if no, change to next state and run it
            if(!room.memory.scouts.includes(creep.name)) {
                room.memory.scouts.push(creep.name);
            }
            creep.moveTo(new RoomPosition(25, 25, creep.memory.roomTarget));
            creep.memory.state = STATE_OBSERVING;
            this.run(creep);
        }
    },
    /**
     * function while observing a room
     * @param {Creep} creep 
     */
    runObserving: function(creep) {
        const room = creep.room;
        const enemies = room.find(FIND_HOSTILE_CREEPS);
        if (enemies.length>0) {
            const nearest = creep.pos.findClosestByRange(enemies);
            if (creep.attack(nearest) == ERR_NOT_IN_RANGE) {
                creep.moveTo(nearest);
            }
        }
        if (creep.ticksToLive<50 && !creep.memory.dyingWish) {
            /** @type {String[]} */
            let scouts = room.memory.scouts;
            scouts.splice(scouts.indexOf(creep.name),1);
            creep.memory.dyingWish = true;
        }
    },
};
module.exports = roleScout;