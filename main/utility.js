/**
 * Created by Rhodan on 28.11.2016.
 */
var _ = require('lodash');
var utility = {
    sleepAllCreeps: function() {
        Game.creeps.map((creep) => creep.memory.sleep)

    },
    /**
     * 
     * @param {Creep} creep 
     */
    findNearestEnergy: function(creep) {
        /** @type {Resource} */
        var energyDrop = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES)
        /** @type {Structure[]} */
        var energyStructures = _.filter(creep.room.find(FIND_MY_STRUCTURES),
        /** @param {Structure} structure */
        (structure) => {
            return (structure.structureType == STRUCTURE_EXTENSION && structure.energy>0)||
            (structure.structureType == STRUCTURE_SPAWN && structure.energy>50) ||
            structure.structureType == STRUCTURE_CONTAINER ||
            structure.structureType == STRUCTURE_STORAGE;
        });
        var nearest = creep.pos.findClosestByRange(energyStructures);
        if (energyDrop == null) {
            return nearest;
        }
        if (nearest == null) {
            return energyDrop;
        }
        if(creep.pos.getRangeTo(energyDrop)>creep.pos.getRangeTo(nearest)) {
            return nearest;
        } else {
            return energyDrop;
        }
    },
    findNearestEnergyThreshold: function(creep,amount) {
        var energyDrop = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES)
        var energyStructures = _.filter(creep.room.find(FIND_MY_STRUCTURES),((structure) => 
        (structure.structureType == STRUCTURE_EXTENSION && structure.energy>=amount) ||
        (structure.structureType == STRUCTURE_SPAWN && structure.energy>250) || 
        (structure.structureType == STRUCTURE_CONTAINER && structure.energy>=amount)|| 
        (structure.structureType == STRUCTURE_STORAGE && structure.energy>=amount)))
        var nearest = creep.pos.findClosestByRange(energyStructures)
        if(creep.pos.getRangeTo(energyDrop)>creep.pos.getRangeTo(nearest)) {
            return nearest
        } else {
            return energyDrop
        }
    },
    findNearestEnergyStored: function(creep,amount) {
        var energyStructures = _.filter(creep.room.find(FIND_MY_STRUCTURES),((structure) => 
        (structure.structureType == STRUCTURE_EXTENSION && structure.energy>=50) ||
        (structure.structureType == STRUCTURE_SPAWN && structure.energy>200+amount) || 
        (structure.structureType == STRUCTURE_CONTAINER && structure.energy>=amount)|| 
        (structure.structureType == STRUCTURE_STORAGE && structure.energy>=amount)))
        var nearest = creep.pos.findClosestByRange(energyStructures)
            return nearest
    },
    findEnergyStored: function(creep,amount) {
        // console.log(creep.room.storage.store[RESOURCE_ENERGY]+"/"+creep.room.energyCapacityAvailable)
        if(creep.room.storage != undefined) {
            if(creep.room.storage.store[RESOURCE_ENERGY]>creep.room.energyCapacityAvailable) {
                return creep.room.storage
            }
        }
        if(creep.room.energyAvailable>creep.room.energyCapacityAvailable-100) {
            var energyStructures = _.filter(creep.room.find(FIND_MY_STRUCTURES),((structure) => 
                (structure.structureType == STRUCTURE_EXTENSION && structure.energy>=50) ||
                (structure.structureType == STRUCTURE_SPAWN && structure.energy>200+amount) || 
                (structure.structureType == STRUCTURE_CONTAINER && structure.energy>=amount)|| 
                (structure.structureType == STRUCTURE_STORAGE && structure.energy>=amount)))
            var nearest = creep.pos.findClosestByRange(energyStructures)
                return nearest
        } else {
            return null
        }
        
    },
    /**
     * 
     * @param {Creep} creep 
     * @param {Number} amount 
     */
    findEnergy: function(creep,amount) {
        // console.log(creep.room.storage.store[RESOURCE_ENERGY]+"/"+creep.room.energyCapacityAvailable)
        if(creep.room.storage != undefined) {
            if(creep.room.storage.store[RESOURCE_ENERGY]>creep.room.energyCapacityAvailable) {
                return creep.room.storage;
            }
        }
        if(creep.room.energyAvailable>creep.room.energyCapacityAvailable-100) {
            var nearest = this.findNearestEnergy(creep);
                return nearest;
        } else {
            return null
        }
        
    },
    /**
     * Find a structure from a given position wich fullfills the given filter function
     * @param {RoomPosition} pos The position from which to find
     * @param {Function} filter The filter function to apply
     */
    findFromStructureFilter(pos,filter) {
        const room = Game.rooms[pos.roomName];
        return room.find(FIND_STRUCTURES,filter);

    },
    /**
     * Find a the nearest structure from a given position wich fullfills the given filter function
     * @param {RoomPosition} pos The position from which to find
     * @param {Function} filter The filter function to apply
     */
    findNearestFromStructureFilter: function(pos,filter) {
        const structures = this.findFromStructureFilter(pos,filter);
        return pos.findClosestByRange(structures);
    },
    /**
     * 
     * @param {Creep} creep 
     */
    collectNearestEnergy: function(creep) {
        var target = this.findNearestEnergy(creep);
        var result;
        if (target == null) {
            creep.moveToHome();
            return null;
        }
        if(target.structureType == undefined){
            result = creep.pickup(target);

        } else {
            result = creep.withdraw(target,RESOURCE_ENERGY);
        }
        switch(result) {
            case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
            case OK: creep.memory.empty = false;break;
        }

    },
    collectNearestEnergyStored: function(creep) {
        let target = this.findNearestEnergyStored(creep)
        let result;
        if(target!=null){
            result = creep.withdraw(target,RESOURCE_ENERGY)

        }
        switch(result) {
            case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
            case OK: creep.memory.empty = false
        }

    },
    collectEnergyStored: function(creep) {
        let target = this.findEnergyStored(creep,creep.carryCapacity)
        let result = undefined
        if(target!=null){
            result = creep.withdraw(target,RESOURCE_ENERGY)

        }
        switch(result) {
            case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
            case OK: creep.memory.empty = false
        }

    },
    collectEnergy: function(creep,amount) {
        let target = this.findEnergy(creep,creep.carryCapacity)
        let result = undefined
        if(target!=null){
            result = creep.withdraw(target,RESOURCE_ENERGY)

        }
        switch(result) {
            case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
            case OK: creep.memory.empty = false
        }

    },
    /**
     * 
     * @param {Creep} creep 
     * @param {Number} amount 
     * @param {Number} minEnergy 
     */
    collectFromStorage: function(creep,amount,minEnergy) {
        let target = creep.room.storage;
        if(target != undefined && target.store[RESOURCE_ENERGY]>minEnergy) {
            if(creep.withdraw(target,RESOURCE_ENERGY,amount) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }   
        }
    },
    collectAllFromStorage: function(creep) {
        let target = creep.room.storage
        if(creep.withdraw(target,RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target)
        }
    },
    chartRoom: function(roomName) {
        if(Memory.roomIds.indexOf(roomName) ==(-1)) {
            Memory.roomIds.push(roomName)
            let room = {}
            room.id = roomName
            room.sources = [];


            let r = Game.rooms[roomName];
            room.sources = this.chartRoomSources(r)


            room.sources.push;




            Memory.rooms.push(room);

        }

    },
    chartRoomSources: function(room) {
        let sources = [];
        let rawSources = room.find(FIND_SOURCES);
    },
    findNearestPosToSpawn: function(oPos) {
        let nPos = oPos.findPathTo(oPos.findClosestByPath(FIND_MY_SPAWNS))[0];
        let rPos = new RoomPosition(nPos.x,nPos.oPos.roomName);
        return rPos;
    },
    findCreepsInRoom: function(roomToSearch) {
        return _.filter(Game.creeps, (creep) => creep.room.name == roomToSearch);
    },
    /**
     * Find the nearest spawn from a given position
     * @param {RoomPosition} pos The position from which
     */
    findNearestSpawn: function(pos) {
        let result = {};
        /** @type {String} */
        result.id = "";
        /** @type {String} */
        result.room = "";
        /** @type {Number} */
        result.distance = 9999;
        for (const spawnId in Game.spawns) {
            const spawn = Game.spawns[spawnId];
            // console.log(JSON.stringify(spawn))
            const spawnRoom = spawn.room.name;
            // console.log("/"+pos.roomName+"_"+spawnRoom);
            let roomDistance = Game.map.findRoute(pos.roomName,spawnRoom);
            // console.log(roomDistance+"/"+roomDistance.length);
            if(roomDistance.length<result.distance) {
                result.distance = roomDistance.length;
                result.id = spawn.id;
                result.room = spawn.room.name;
            }
        }
        return result;
    },
    /**
     * Find the nearest storage from a given position
     * @param {RoomPosition} pos The position from which to find
     */
    findNearestStorage: function(pos) {
        let result = {};
        result.id = "";
        result.distance = 9999;
        for (const roomName in Game.rooms) {
            let room = Game.rooms[roomName];
            if(room.storage != undefined) {
                let roomDistance = Game.map.findRoute(pos.roomName,roomName);
                if(roomDistance.length<result.distance) {
                    result.distance = roomDistance.length;
                    result.id = room.storage.id;
                    result.room = room.name;
                }

            }
        }
        return result;
    },
    
};
module.exports = utility;