var structureExtension = function() {
    Object.defineProperty(Structure.prototype, 'memory', {
        configurable: true,
        get: function() {
            if(_.isUndefined(Memory.structures)) {
                Memory.structures = {};
            }
            if(!_.isObject(Memory.structures)) {
                return undefined;
            }
            return Memory.structures[this.id] = Memory.structures[this.id] || {};
        },
        set: function(value) {
            if(_.isUndefined(Memory.structures)) {
                Memory.structures = {};
            }
            if(!_.isObject(Memory.structures)) {
                throw new Error('Could not set Structure memory');
            }
            Memory.structures[this.id] = value;
        }
    });
    Structure.prototype.getDistanceToNearestSpawn = function() {
        var distance = 9999;
        var spawnId;

        for (const sp in Game.spawns) {
            let spawn = Game.spawns[sp];
            let res = PathFinder.search(this.pos, spawn.pos).cost;
            if (res < distance) {
                distance = res;
                spawnId = spawn.id;
            }

        }
        return {spawnId,distance};
    }
    Structure.prototype.getDistanceToNearestStorage = function() {
        var room = Game.rooms[this.pos.roomName]
        var id = room.storage.id;
        var distance = PathFinder.search(this.pos, room.storage.pos).cost;
        return {id,distance};
    }
    
};
module.exports = structureExtension;

// Extend structures with memory
