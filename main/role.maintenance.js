/**
 * Created by Rhodan on 28.11.2016.
 */
var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleMaintenance = {
    run: function(creep) {
        if(creep.memory.empty ==undefined) {
            creep.memory.empty = true;
        }
        if(!creep.memory.empty) {
            var target = undefined
            if(creep.memory.target==undefined) {
                var damaged = _.filter(creep.room.find(FIND_STRUCTURES), (structure) =>
                structure.hits * 2 < structure.hitsMax && structure.hits < 50000)
                target = creep.pos.findClosestByRange(damaged)
                if(target!=null) {
                    creep.memory.target = target.id
                }

            } else {
                target = Game.getObjectById(creep.memory.target)
            }
            var result = creep.repair(target)
            switch(result) {
                case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
                case ERR_NOT_ENOUGH_RESOURCES: creep.memory.empty = true;creep.memory.target=undefined;break;
            }
            if(target.hits==target.hitsMax) {
                creep.memory.target=undefined
            }
        } else {
            util.collectNearestEnergy(creep)
        }
    },
    build: function(spawn) {
        if(spawn.room.energyCapacityAvailable>450) {
            
        }
        spawn.createCreep([WORK,CARRY,CARRY,MOVE,MOVE], undefined,
                    {role: 'maintenance', empty: true, sleep: false});
    }
}
module.exports = roleMaintenance