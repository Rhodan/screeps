/**
 * Created by Rhodan on 28.11.2016.
 */
var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleMiningHauler = {
    
    genericTemplate: [CARRY,CARRY,MOVE],
    
    run: function (creep) {
        // Check if Creep is empty or is full
        if (creep.carry.energy == 0) {
            // Creep is empty
            
            // check if creep already has an hauling job
            if(creep.memory.target==undefined) {
                // has no target
                
                // if not, find a container with enough energy
                /** @type {StructureContainer} */
                var targets = creep.room.find(FIND_STRUCTURES, {
                    /** @param {Structure} structure */
                    filter: (structure) => {
                        if(structure.structureType == STRUCTURE_CONTAINER) {
                            if(structure.memory.reserved==undefined) {
                                structure.memory.reserved = 0;
                            }
                        }
                        return (structure.structureType == STRUCTURE_CONTAINER) &&
                        structure.store[RESOURCE_ENERGY]-structure.memory.reserved>creep.carryCapacity;
                    }
                });
                if (targets.length > 0) {
                    /** @type {StructureContainer} */
                    var target = creep.pos.findClosestByRange(targets)
                    creep.memory.target = target.id;
                    // console.log("Creep"+creep.name+" reserved "+creep.carryCapacity+" at "+target+
                    // " with "+target.store[RESOURCE_ENERGY]+"/"+target.memory.reserved)
                    target.memory.reserved += creep.carryCapacity;
                }
            } else {
                // already has target
                
                // move to target and withdraw energy
                var target = Game.getObjectById(creep.memory.target)
                // check if dying
                if(creep.ticksToLive<4) {
                    
                    // free Reserved Energy and delete Target
                    target.memory.reserved -= creep.carryCapacity;
                    creep.memory.target=undefined;
                    creep.suicide();
                }
                
                // check if the containers energy levels have fallen below capacity
                if(target.store[RESOURCE_ENERGY]<creep.carryCapacity) {
                    creep.memory.target=undefined;
                    target = undefined;
                }
                
                // try to withdraw energy, move to target if out of range
                if (creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                } else {
                    
                }
                
            }


        } else {
            // Creep is full
            
            // check if creep has still a target=>just withdrew Energy
            if(creep.memory.target!=undefined) {
                // still has target
                
                // cancel reservation and delete target
                var target = Game.getObjectById(creep.memory.target);
                // console.log("Creep"+creep.name+" freed "+creep.carryCapacity+" at "+target+
                //     " with "+target.store[RESOURCE_ENERGY]+"/"+target.memory.reserved);
                target.memory.reserved -= creep.carryCapacity;
                creep.memory.target=undefined;
            }
            
            // find low-level structures to put energy into
            var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_STORAGE) && structure.energy < structure.energyCapacity;
                }
            });
            if (targets.length > 0) {
                // found structures to put energy into 
                
                var nearest = creep.pos.findClosestByRange(targets);
                if (creep.transfer(nearest, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(nearest);
                } else {
                    creep.memory.target = undefined;
                }
            } else {
                // 
                
                if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.storage);
                } else {
                    creep.memory.target = undefined;
                }
            }
        }
    },
    
    build: function(spawn) {
        if(spawn.room.energyCapacityAvailable>600) {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE], undefined, {role: 'miningHauler'})
        } else {
            spawn.createCreep([CARRY,CARRY,CARRY,CARRY,MOVE,MOVE], undefined, {role: 'miningHauler'})
        }
    }
}
module.exports = roleMiningHauler
