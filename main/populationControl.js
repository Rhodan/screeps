//  @ts-check
/**
 * Created by Rhodan on 04.12.2016.
 */
// Jobs
var roleNames = ["harvester","miner","extractor","commandSupplier","towerSupplier","builder","hauler","miningHauler","containerHauler","distributor","maintenance","upgrader","scout","reserver"];
var roles = {};
for(var role of roleNames) {
    roles["role_"+role] = require("role."+role);
}
// util
var _ = require('lodash');

var populationControl =  {
    
    check: function() {
        for(var spawn in Game.spawns) {
            this.populate(Game.spawns[spawn])
        }
        
    },
    populate: function(spawn) {
            if((     _.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'distributor').length < 1) && spawn.room.storage.store[RESOURCE_ENERGY]>1000) {
                roles["role_distributor"].build(spawn)
            }
            if((     _.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'miner').length < 1)&&_.filter(Game.creeps, (creep) => creep.memory.role == 'harvester').length < 1) {
                roles["role_harvester"].build(spawn)
            }
            if((     _.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'miner').length < 1)) {
                roles["role_miner"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'hauler').length < 1)) {
                roles["role_hauler"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'miner').length < 2)) {
                roles["role_miner"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'hauler').length < 2)) {
                roles["role_hauler"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'commandSupplier').length < 1)) {
                roles["role_commandSupplier"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'builder').length < 1)) {
                roles["role_builder"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'distributor').length < 1) && spawn.room.energyCapacityAvailable>300) {
                roles["role_distributor"].build(spawn)
            }
            else if(_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'containerHauler').length < spawn.room.neededContainerHaulers()+1) {
                roles["role_containerHauler"].build(spawn)
            } 
            else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'builder').length < 2)&& spawn.room.energyAvailable>400) {
                roles["role_builder"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'commandSupplier').length < 2)&& spawn.room.energyAvailable>600) {
                roles["role_commandSupplier"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'extractor').length < 1) && spawn.room.find(FIND_MINERALS)[0].mineralAmount>0) {
                roles["role_extractor"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'distributor').length < 2) && spawn.room.energyCapacityAvailable>1200) {
                roles["role_distributor"].build(spawn)
            }
            else if((_.filter(Game.creeps, (creep) => creep.room == spawn.room && creep.memory.role == 'commandSupplier').length < 6)&& spawn.room.energyAvailable>600 && 
            spawn.room.storage.store[RESOURCE_ENERGY]>200000) {
                roles["role_commandSupplier"].build(spawn)
            }
            // else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'towerSupplier').length < 2)) {var newName = Game.spawns['Home'].createCreep([CARRY,CARRY,CARRY,CARRY,MOVE,MOVE], undefined, {role: 'towerSupplier'});}
            // else if(Game.spawns['Home'].room.energyAvailable>350&&(_.filter(Game.creeps, (creep) => creep.memory.role == 'maintenance').length < 1)) {var newName = Game.spawns['Home'].createCreep([WORK,WORK,CARRY,MOVE,MOVE], undefined, {role: 'maintenance'});}
            // else if(Game.spawns['Home'].room.energyAvailable>350&&(_.filter(Game.creeps, (creep) => creep.memory.role == 'commandSupplier').length < 2)) {var newName = Game.spawns['Home'].createCreep([WORK,WORK,CARRY,MOVE,MOVE], undefined, {role: 'commandSupplier'});}
            // else if(Game.spawns['Home'].room.energyAvailable>350&&(_.filter(Game.creeps, (creep) => creep.memory.role == 'builder').length < 2)) {var newName = Game.spawns['Home'].createCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE], undefined, {role: 'builder'});}

        

    },
    //  park:function() {
    //      if((_.filter(Game.creeps, (creep) => creep.memory.role == 'harvester').length < 4)) {
    //              var newName = Game.spawns['Spawn1'].createCreep([WORK,WORK,CARRY,MOVE], undefined,
    //                  {role: 'harvester', empty: true, sleep: false});
    //          }
    //          else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader').length < 2)) {
    //              var newName = Game.spawns['Spawn1'].createCreep(roleUpgrader.template, undefined,
    //                  {role: 'upgrader', empty: true, sleep: false});
    //          }
    //          else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'distributor').length < 1 && Game.spawns['Spawn1'].room.energyCapacityAvailable>300)) {
    //              var newName = Game.spawns['Spawn1'].createCreep(roleDistributor.template, undefined,
    //                  {role: 'distributor', empty: true, sleep: false});
    //          }
    //          else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'hauler').length < 2)) {
    //              var newName = Game.spawns['Spawn1'].createCreep(roleDistributor.template, undefined,
    //                  {role: 'hauler', empty: true, sleep: false});
    //          }
    //          else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'builder').length < 2)) {
    //              var newName = Game.spawns['Spawn1'].createCreep([WORK,CARRY,CARRY,MOVE,MOVE], undefined,
    //                  {role: 'builder', empty: true, sleep: false});
    //          }
    //          else if((_.filter(Game.creeps, (creep) => creep.memory.role == 'maintenance').length < 1)) {
    //              var newName = Game.spawns['Spawn1'].createCreep([WORK,WORK,CARRY,MOVE], undefined,
    //                  {role: 'maintenance', empty: true, sleep: false});
    //          }
    //  }
}
module.exports = populationControl;