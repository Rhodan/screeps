/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('memoryManager');
 * mod.thing == 'a thing'; //  true
 */
var util = require('./utility')
var _ = require("lodash");

var memoryManager = {

    buildRooms: function(){
        for(var r in Game.rooms){
            Game.rooms[r].memory.sources = Game.rooms[r].find(FIND_SOURCES)
        }
    },
    insertStructures: function(){
        // iterate through all owned structures
        for(const structureId in Game.structures) {
            const structure = Game.structures[structureId];
            // add all non-rampart structures to Memory.structures
            if(structure.structureType != STRUCTURE_RAMPART) {
                if(Memory.structures[structureId] == undefined) {
                    Memory.structures[structureId] = {};
                    if(structure.structureType==STRUCTURE_CONTAINER) {
                        structure.memory.reserved = 0;
                    }
                };
                Memory.structures[structureId].type = structure.structureType;
                Memory.structures[structureId].room = structure.room.name;
            }
        }
    },
    cleanRootMemory: function() {
        // Clean Structures Table
        for(const structureId in Memory.structures) {
            /** @type {Structure} */
            let structure = Game.getObjectById(structureId);
            if(structure == undefined) {
                delete Memory.structures[structureId]
            }
        }
    },
    cleanStructures: function() {
        for(const structureId in Memory.structures) {
            const structure = Game.getObjectById(structureId);
            if(structure.structureType == STRUCTURE_CONTAINER) {
                let creeps = _.filter(Game.creeps,(creep) => {
                    return creep.memory.role=="containerHauler" && creep.memory.target == structure.id;
                });
                let reservedAmount = 0;
                for(const creep of creeps) {
                    reservedAmount += creep.carryCapacity;
                }
                structure.memory.reserved = reservedAmount;
            }
        }
    },
}
module.exports = memoryManager;