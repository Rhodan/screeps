/**
 * Created by Rhodan on 04.12.2016.
 */
var util = require("utility");
var utilCreeps = require("utility.creeps");
var roleUpgrader =  {
    template:  [WORK,CARRY,MOVE],
    run: function(creep) {
        if(creep.memory.empty) {
            var target = Game.getObjectById('5bbcae399099fc012e638993')
            switch(creep.harvest(target)) {
                case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
                case OK: creep.upgradeController(creep.room.controller);break;
            }
            if(creep.carry.energy==creep.carryCapacity) {
                creep.memory.empty= false;
            }

        } else {
            var target = creep.room.controller
            switch(creep.upgradeController(target)) {
                case ERR_NOT_IN_RANGE: creep.moveTo(target);break;
                case OK: if(creep.carry.energy==0) {creep.memory.empty=true};break;
            }
            if(creep.carry.energy==0) {creep.memory.empty=true}
        }
    }
}
module.exports = roleUpgrader