var _ = require('lodash');
var roomExtension = function () {
    /**
     * @returns {Structure[]}
     */
    Room.prototype.getStructures = function () {
        if (this.structures == undefined) {
            this.structures = this.find(FIND_STRUCTURES);
        }
        return this.structures;
    }
    /**
     * @name Room#getActiveStructures
     * @memberof Room
     * @returns {Structure[]}
     */
    Room.prototype.getActiveStructures = function () {
        if (this.activeStructures == undefined) {
            const passives = [STRUCTURE_WALL, STRUCTURE_ROAD, STRUCTURE_RAMPART];
            this.activeStructures = _.filter(this.getStructures(), (structure) => {
                return !passives.includes(structure.structureType);
            });
        }
        return this.activeStructures;

    };
    /**
     * @returns {Source[]}
     */
    Room.prototype.getSources = function () {
        
        if (this.sources == undefined) {
            this.sources = this.find(FIND_SOURCES);
        }
        return this.sources;
    };
    /**
     * @memberof! Room
     * @returns {StructureContainer[]}
     */
    Room.prototype.getContainers = function () {
        if (this.containers == undefined) {
            this.containers = _.filter(this.getActiveStructures(),
                (structure) => {
                    return structure.structureType == STRUCTURE_CONTAINER;
                }
            );
        }
        return this.containers;
    };
    /**
     * @return {StructureLink[]}
     */
    Room.prototype.getLinks = function () {
        if (this.links == undefined) {
            this.links = _.filter(this.getActiveStructures(),
                /** @param structure {Structure} */
                (structure) => {
                    return structure.structureType == STRUCTURE_LINK;
                }
            );
        }
        return this.links;
    }
    /**
    * Labes the links in this roomdepending on position
    * @param {StructureLink[]} links 
    * @param {Room} room 
    */
    Room.prototype.labelLinks = function (links) {
        for (const link of links) {
            if (link.memory.role != undefined) return null;
            if (link.pos.findInRange(FIND_SOURCES, 2).length > 0) {
                link.memory.role = "linkSource";
            }
            if (link.pos.findInRange(FIND_STRUCTURES, 2, { filter: { "structureType": STRUCTURE_STORAGE } }).length > 0) {
                link.memory.role = "linkStorage";
            }
        }
        return "checked links";
    }
    /**
     * Label the containers in this room depending on position
     * @param {StructureContainer[]} containers 
     * @param {Room} room 
     */
    Room.prototype.labelContainers = function (containers) {
        for (const container of containers) {
            if (container.memory.role != undefined) return null;
            if (container.pos.findInRange(FIND_SOURCES, 2).length > 0) {
                container.memory.role = "containerSource";
            }
            if (container.pos.findInRange(FIND_MINERALS, 1).length > 0) {
                container.memory.role = "containerMineral";
            }
            if (container.pos.inRangeTo(this.controller, 3)) {
                container.memory.role = "containerController";
            }
        }
        return "checked containers";

    }
    /**
     * calculate number of sources with container but no link
     * @returns {number}
     */
    Room.prototype.neededContainerHaulers = function () {
        const containers = _.filter(this.getContainers(),
            /** @param structure {StructureContainer} */
            (structure) => {
                return structure.memory.role == "containerSource" ||
                    (structure.memory.role == "containerMineral" && structure.store[structure.getTopResource()] > 1000);
            });
        const links = _.filter(this.getLinks(),
            /** @param structure {StructureLink} */
            (structure) => {
                return structure.memory.role == "linkSource";
            });
        const result = containers.length - links.length;
        return result;

    };
    /**
     * Returns the container associated with the room controller or undefined if there is none
     * @return {StructureContainer}
     */
    Room.prototype.getControllerContainer = function() {
        for (const container of this.getContainers()) {
            if (container.memory.role == "containerController") {
                return container;
            }
        }
        return undefined;
    }
}
module.exports = roomExtension;