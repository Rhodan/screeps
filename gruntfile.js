
module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-screeps');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.initConfig({
        copy: {
            main: {
                expand: true,
                cwd: "main/",
                src: "*.js",
                dest: "dist/"
            }
        },
        screeps: {
            options: {
                email: 'rhodan-spiele@t-online.de',
                password: 'q.7Run3xwZhHiPB',
                branch: 'main',
                ptr: false
            },
            dist: {
                src: ['dist/*.js']
            }
        }
    });
    grunt.registerTask("screeps-push",["copy","screeps"]);

}